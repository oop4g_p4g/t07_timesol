// Time.h: interface for the Time class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(TimeH)
#define TimeH


#include <cassert>	//for assert
#include <iostream>	//for cin >>, cout << and random number routines
#include <iomanip>	//for setw and setfill
#include <ctime>	//for time used in random number routines
#include <string>	//for string
#include <sstream>	//for string stream processing
using namespace std;

//--------Time class
class Time {
public:
	//constructors & destructor
	Time();				//default constructor
	Time( const Time&);	//copy constructor
	Time( int h, int m, int s);	//simple constructor
	explicit Time( long);			//conversion constructor
	explicit Time( const string&);	//conversion constructor
	~Time();		//destructor

	//public functions
	int getHours() const;	//return data member value, hours_
	int getMinutes() const;	//return data member value, minutes_
	int getSeconds() const;	//return data member value, seconds_
	void setTime( int h, int m, int s); 	//set to given values
	void setTime( long lt); 	//set time from long value
	void setTime( string strt); 	//set time from string value ("HH:MM:SS")
	void setTimeFromSystem(); 	//set to system time
	void setTimeFromUserInput();	//read in formatted digital time (as HH:MM:SS)
	void printTime() const;	//print out formatted digital time (as HH:MM:SS)
	bool isValid() const; 	//check if is valid
	bool isSameAs( const Time& t) const; 	//check if same as t
	bool isDifferentFrom( const Time& t) const; 	//check if not same as t
	bool isEarlierThan( const Time& t) const;	//check strictly earlier than t
	bool isLaterThan( const Time& t) const;	//check strictly later than t
	string toFormattedString() const;	//return formatted string ("HH:MM:SS")
	long toLong() const;   	//return time in long secs
	//ostream& Time::toStream( ostream& os) const;	//____TO BE CHECKED!
	//istream& Time::fromStream( istream& is);		//____TO BE CHECKED!

	//operator overloading
	bool operator==( const Time& t) const; 	//check if same as t
	bool operator!=( const Time& t) const; 	//check if not same as t
	bool operator<( const Time& t) const;	//check strictly earlier than t
	bool operator>=( const Time& t) const;	//later than or equal to t
	bool operator>( const Time& t) const;	//strictly later than t
	bool operator<=( const Time& t) const;	//earlier than or equal to t
	void operator!();	//reset time
	const Time operator+( long secs) const;	//addition operator with long
//	Time operator+( const Time& t) const;   	//addition operator with Time
	Time& operator++();	//prefix increment
	const Time operator++( int secs);	//postfix increment
	Time& operator--();	//prefix decrement
	const Time operator--( int secs);	//postfix decrement
	const Time operator-() const;	//minus operator defining time from midnight
	operator long() const ;	//cast operator
	operator string() const;	//cast operator
private:
	int hours_, minutes_, seconds_;
};

//binary operators defined outside the class
const Time operator+( long secs, const Time& t);

ostream& operator<<( ostream& os, const Time& t);
istream& operator>>( istream& is, Time& t);

#endif // !defined(TimeH)
