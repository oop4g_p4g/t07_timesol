////////////////////////////////////////////////////////////////////////
// OOP Tutorial Time: Operator Overloading (solution)
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <iostream>	//for cin >>, cout <<
#include <string>	//for string
using namespace std;

//--------include Time class declarations
#include "Time.h"

//--------start program
int main()
{
	/*
	//basic version

	Time t1;
	t1.setTime(12, 34, 56);
	cout << "\n t1 hours are " << t1.getHours();
	cout << "\n t1 minutes are " << t1.getMinutes();
	cout << "\n t1 seconds are " << t1.getSeconds();
	cout << "\n t1 is ";
	t1.printTime();

	t1.setTime( "12:34:57");
	cout << "\n t1 is ";
	t1.printTime();

	if ( t1.isValid())
		cout << "\n t1 is valid";
	else
		cout << "\n t1 is NOT valid";

	Time t2;
	t2.setTimeFromSystem();
	cout << "\n t2 is ";
	t2.printTime();

	Time t3;
	cout << "\n Enter values for t3 (HH:MM:SS) ";
	t3.setTimeFromUserInput();
	cout << "\n t3 is ";
	t3.printTime();
	if ( t3.isValid())
		cout << "\n t3 is valid";
	else
		cout << "\n t3 is invalid";

	if ( t2.isSameAs( t3))
		cout << "\n t2 and t3 are identical";
	else
		cout << "\n t2 and t3 are NOT identical";

	cout << "\n t2.isEarlierThan( t3) is ";
	if ( t2.isEarlierThan( t3))
		cout << "true";
	else
		cout << "false";
	cout << "\n t2.isLaterThan( t3) is ";
	if ( t2.isLaterThan( t3))
		cout << "true";
	else
		cout << "false";

	Time t4(300); 	//create time from long value
	cout << "\n t4 (from long 300 using toFormattedString()) is ";
	string s( t4.toFormattedString());
	cout << s;

	Time t5("00:10:10"); 	//create time from string value
	cout << "\n t5 (from string \"00:10:10\" using toLong()) is ";
	long l( t5.toLong());
	cout << l;

	*/
		
	//testing operators
	
	//Question 2
	//Time t1( 12, 30, 0);
	//Time t2( 12, 30, 0);
	//if ( t1 == t2) 
	//	cout << "\n t1 and t2 are identical";
	//else
	//	cout << "\n t1 and t2 are NOT identical";

	//if ( t1 != t2) 
	//	cout << "\n t1 and t2 are NOT identical";
	//else
	//	cout << "\n t1 and t2 are identical";

	//if ( t1 < t2)
	//	cout << "\n t1 is earlier than t2 ";
	//else
	//	cout << "\n t1 is NOT earlier than t2";

	//if ( t1 <= t2)
	//	cout << "\n t1 is earlier than or equal to t2 ";
	//else
	//	cout << "\n t1 is NOT earlier than or equal to t2";

	//	if ( t1 > t2)
	//	cout << "\n t2 is earlier than t1 ";
	//else
	//	cout << "\n t2 is NOT earlier than t1";

	//if ( t1 >= t2)
	//	cout << "\n t2 is earlier than or equal to t1 ";
	//else
	//	cout << "\n t2 is NOT earlier than or equal to t1";

	////Question 3
	//Time t1( 0, 1, 20);
	//Time t2( 10, 11, 26);
	//t1 = t2;
	//t1.printTime();	//output 10:11:26

 // 	//Question 4a
	//Time t( 01, 0, 0);
	//t = t + 12;	//set t to 01:00:12
	//cout << t.toFormattedString() << endl;	//output "01:00:12"

	//t = 12 + t;	//ERROR with only const Time Time::operator*( long)!
	//			//OK with const Time operator*( long, const Time&)!

	//t.setTime( 01, 0, 0);
	//cout << t.toFormattedString() << endl;	//output "01:00:00"
	//cout << t++.toFormattedString() << endl;	//output "01:00:00"
	//cout << t.toFormattedString() << endl;	//output "01:00:01"
	//cout << ++t.toFormattedString() << endl;	//output "01:00:02"
	//cout << t.toFormattedString() << endl;	//output "01:00:02"
	//cout << t--.toFormattedString() << endl;	//output "01:00:02"
	//cout << t.toFormattedString() << endl;	//output "01:00:01"
	//cout << --t.toFormattedString() << endl;	//output "01:00:00"
	//cout << t.toFormattedString() << endl;	//output "01:00:00"


 // 	//Question 4b
	//Time t( 01, 0, 0);
	//cout << t.toFormattedString() << endl;	//output "01:00:00"
	//cout << (t++).toFormattedString() << endl;	//output "01:00:00"
	//cout << t.toFormattedString() << endl;	//output "01:00:01"
	//cout << (++t).toFormattedString() << endl;	//output "01:00:02"
	//cout << t.toFormattedString() << endl;	//output "01:00:02"
	//cout << (t--).toFormattedString() << endl;	//output "01:00:02"
	//cout << t.toFormattedString() << endl;	//output "01:00:01"
	//cout << (--t).toFormattedString() << endl;	//output "01:00:00"
	//cout << t.toFormattedString() << endl;	//output "01:00:00"

 // 	//Question 4c
	//Time t1( 1, 0, 0), t2( 1, 10, 05);
	//cout << (-t1).toFormattedString() << endl;	//output "23:00:00"
	//cout << (-t2).toFormattedString() << endl;	//output "22:49:55"

	////Question 5
	//Time t;
	//cout << "\n Enter values for t (HH:MM:SS) ";
	//cin >> t;
	//cout << "\n t is now " << t;

  	//Question 6
	Time t( 1, 2, 3);
	cout << "\n" <<  long( t);	//display 3723
	cout << "\n" << static_cast<long>( t); 	//ditto 

	cout << "\n" << string( t);	//display "01:02:03" 
	cout << "\n" << static_cast<string>( t); 	//ditto


	cout << "\n\nPress any key to continue...";
	system("PAUSE");
	return 0;
}
