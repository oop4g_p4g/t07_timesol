// Time.cpp: implementation of the Time class.
//
//////////////////////////////////////////////////////////////////////

#include "Time.h"


// constructors & destructors
Time::Time() {
	hours_   = 0;
	minutes_ = 0;
	seconds_ = 0;
}
Time::Time( const Time& t) {
	assert( t.isValid());
	hours_ = t.hours_;
	minutes_ = t.minutes_;
	seconds_ = t.seconds_;
}
Time::Time( int h, int m, int s) {
	assert((h >= 0) && (h < 24) && (m >= 0) && (m < 60) && (s >= 0) && (s < 60));
	hours_ = h;
	minutes_ = m;
	seconds_ = s;
}
Time::Time( long lt) {
	assert ( lt >= 0);
	hours_   = ((lt / 60) / 60) % 24;
	minutes_ = (lt / 60) % 60;
	seconds_ = lt % 60;
}
Time::Time( const string& strt) {
	istringstream is_time( strt);
	char ch;			//(any) colon field delimiter
	is_time >> hours_ >> ch >> minutes_ >> ch >> seconds_;
}
Time::~Time() {
}


//public functions
int Time::getHours() const {
	return hours_;
}
int Time::getMinutes() const {
	return minutes_;
}
int Time::getSeconds() const {
	return seconds_;
}
void Time::setTime( int h, int m, int s) {
//pre: h:m:t is a valid time
	Time t;
	t.hours_ = h;
	t.minutes_ =  m;
	t.seconds_ =  s;
	assert( t.isValid());
	hours_ = t.hours_;
	minutes_ = t.minutes_;
	seconds_ = t.seconds_;
}
void Time::setTime( long secs) {
//pre: secs >= 0 and represents a valid time
	assert( secs >= 0);
	Time t;
	const long totalMinutes = secs / 60;
	t.hours_ = ( totalMinutes / 60) % 24;
	t.minutes_ = totalMinutes % 60;
	t.seconds_ = secs % 60;
	assert( t.isValid());
	hours_ = t.hours_;
	minutes_ = t.minutes_;
	seconds_ = t.seconds_;
}
void Time::setTime( string strt) {
//pre: str represents a valid time
	istringstream is_time( strt);
	char ch;			//(any) colon field delimiter
	is_time >> hours_ >> ch >> minutes_ >> ch >> seconds_;
}
void Time::setTimeFromSystem() {
//set to system time
	time_t now = time(0);
	struct tm t;
	if (localtime_s(&t, &now) != 0)
		assert(false);
	hours_   = t.tm_hour ;
	minutes_ = t.tm_min ;
	seconds_ = t.tm_sec ;
}
void Time::setTimeFromUserInput() {
//read in a formatted digital time (HH:MM:SS) - assume user enter valid data
	char ch;	//(any) colon field delimiter
	cin >> hours_ >> ch >> minutes_ >> ch >> seconds_;
}
void Time::printTime() const {
//print out formatted digital time (HH:MM:SS)
	cout << setfill('0');
	cout << setw(2) << hours_ << ":";
	cout << setw(2) << minutes_ << ":";
	cout << setw(2) << seconds_;
	cout << setfill(' ');
}
bool Time::isValid() const {
	return (( hours_ >= 0) && ( hours_ < 24) &&
	        ( minutes_ >= 0) && ( minutes_ < 60) &&
	        ( seconds_ >= 0) && ( seconds_ < 60));
}
bool Time::isSameAs( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	return (( hours_ == t.hours_) &&
	        ( minutes_ == t.minutes_) &&
	        ( seconds_ == t.seconds_));
}
bool Time::isDifferentFrom( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	return ( ! ( isSameAs( t)));		//same as return ( ! ( this->isSameAs( t)));
}
bool Time::isEarlierThan( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	bool strictlyEarlier;
	if ( hours_ < t.hours_)
		strictlyEarlier = true;
	else
		if (( hours_ == t.hours_) && ( minutes_ < t.minutes_))
			strictlyEarlier = true;
		else
			if ((hours_ == t.hours_) &&
			    (minutes_ == t.minutes_) &&
			    (seconds_ < t.seconds_))
				strictlyEarlier = true;
			else
				strictlyEarlier = false;
	return (strictlyEarlier);
}
bool Time::isLaterThan( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	return ( t.isEarlierThan( *this));
}
string Time::toFormattedString() const {
	ostringstream os_time;
	os_time << setfill('0');
	os_time << setw(2) << hours_ << ":";
	os_time << setw(2) << minutes_ << ":";
	os_time << setw(2) << seconds_;
	return ( os_time.str());
}
long Time::toLong() const {  	//return time in long secs
	return ( ( hours_ * 3600) + ( minutes_ * 60) + seconds_);
}

//---operators overloading

bool Time::operator==( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	return (( hours_ == t.hours_) &&
	        ( minutes_ == t.minutes_) &&
	        ( seconds_ == t.seconds_));
}

bool Time::operator!=( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	return ( ! ( (*this) == t));
}
/* OK
bool Time::operator!=( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	return ( ! ( this->operator ==(t)));
}
*/
/* OK
bool Time::operator!=( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	return ( ! (t == (*this)));
}
*/
/* OK if declared in the Time.h file, outside the class definition
bool operator!=( const Time& t1, const Time& t2) const {
	return ( ! (t1 == t2));
}
*/

bool Time::operator<( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	bool strictlyEarlier;
	if ( hours_ < t.hours_)
		strictlyEarlier = true;
	else
		if (( hours_ == t.hours_) && ( minutes_ < t.minutes_))
			strictlyEarlier = true;
		else
			if ((hours_ == t.hours_) &&
			    (minutes_ == t.minutes_) &&
			    (seconds_ < t.seconds_))
				strictlyEarlier = true;
			else
				strictlyEarlier = false;
	return (strictlyEarlier);
}
bool Time::operator>=( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	return ( ! ((*this) < t));
}
bool Time::operator>( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	return ( t < (*this));
}
bool Time::operator<=( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	return ( ! ( t < (*this)));
}
void Time::operator!() {
	hours_   = 0;
	minutes_ = 0;
	seconds_ = 0;
}

const Time Time::operator+( long secs) const {
	Time tx;
	tx.setTime( secs + this->toLong());
	return ( tx);
}

/*
Time Time::operator+( const Time& t) const {
//pre: t is a valid time
	assert( t.isValid());
	Time tx;
	tx.setTime( t.toLong() + this->toLong());
	return ( tx);
}
*/
Time& Time::operator++() {
	seconds_++;
	if ( seconds_ == 60) {
		seconds_ = 0;
		minutes_++;
		if ( minutes_ == 60) {
			minutes_ = 0;
			hours_++;
			if ( hours_ == 24)
				hours_ = 0;
		}
	}
	return( *this);		//enable cascaded decrements
}
const Time Time::operator++( int) {
	Time temp( *this);
	++(* this);
	return( temp);
}
Time& Time::operator--() {
	seconds_--;
	if ( seconds_ == -1) {
		seconds_ = 0;
		minutes_--;
		if ( minutes_ == -1) {
			minutes_ = 0;
			hours_--;
			if ( hours_ == -1)
				hours_ = 0;
		}
	}
	return( *this);		//enable cascaded decrements
}
const Time Time::operator--( int) {
	Time temp( *this);
	--(* this);
	return( temp);
}
const Time Time::operator-() const {	//minus operator defining time from midnight
	Time t;
	t.setTime( (24 * 3600) - toLong());
	return t;
}
Time::operator long() const {	//cast operator
	return (( hours_ * 3600) + ( minutes_ * 60) + seconds_);
}

Time::operator string() const {	//cast operator
	ostringstream os_time;
	os_time << setfill('0');
	os_time << setw(2) << hours_ << ":";
	os_time << setw(2) << minutes_ << ":";
	os_time << setw(2) << seconds_;
	return ( os_time.str());
}
//--------end Time class


//defined outside the class
const Time operator+( long secs, const Time& t) {
	Time tx;
	tx.setTime( secs + t.toLong());
	return ( tx);
}
ostream& operator<<( ostream& os, const Time& t) {
//output digital clock formatted as HH:MM:SS
	os << setfill('0');
	os << setw(2) << t.getHours() << ":";
	os << setw(2) << t.getMinutes() << ":";
	os << setw(2) << t.getSeconds();
	os << setfill(' ');
	return os;
}
istream& operator>>( istream& is, Time& t) {
//read in time as HH:MM:SS
	int h, m, s;
	char ch; 		//(any) colon field delimiter
	is >> h >> ch >> m >> ch >> s;
	t.setTime( h, m, s);	//set data members of t
	return is;
}

/*
//________________________________________________________________TO BE CHECKED!!!!
ostream& operator<<( ostream& os, const Time& t) {
//output digital clock formatted as HH:MM:SS
	os << t.toStream( os) <<"TEST";
	return os;
}
istream& operator>>( istream& is, Time& t) {
//read in time as HH:MM:SS
	t.fromStream( is);
	return is;
}
istream& Time::fromStream( istream& is) {
//set time from values in stream containing digital clock formatted as HH:MM:SS
	int h, m, s;
	char ch; 		//(any) colon field delimiter
	is >> h >> ch >> m >> ch >> s;
	setTime( h, m, s);	//set data members of this time
	return is;
}
ostream& Time::toStream( ostream& os) const {
//return stream containing digital clock formatted as HH:MM:SS
	os << setfill('0');
	os << setw(2) << getHours() << ":";
	os << setw(2) << getMinutes() << ":";
	os << setw(2) << getSeconds();
	os << setfill(' ');
	return os;
}
//________________________________________________________________UNTIL HERE _ FUNNY OUTPUT!!!!
*/

